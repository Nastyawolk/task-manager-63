<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="../include/_header.jsp" />
<h1>PROJECT LIST</h1>
<table width="100%" border="1" style="border-collapse: collapse">
    <tr>
        <th>ID</th>
        <th>NAME</th>
        <th>DESCRIPTION</th>
        <th>STATUS</th>
        <th>START</th>
        <th>FINISH</th>
        <th>EDIT</th>
        <th>DELETE</th>
    </tr>
    <c:forEach var="project" items="${projects}">
        <tr align="center">
            <td>
                <c:out value="${project.id}"/>
            </td>
            <td>
                <c:out value="${project.name}"/>
            </td>
            <td>
                <c:out value="${project.description}"/>
            </td>
            <td>
                <c:out value="${project.status}"/>
            </td>
            <td>
                <f:formatDate pattern="dd.MM.yyyy" value="${project.dateStart}" />
            </td>
            <td>
                <f:formatDate pattern="dd.MM.yyyy" value="${project.dateFinish}" />
            </td>
            <td>
                <a href="/project/edit/?id=${project.id}">EDIT</a>
            </td>
            <td>
                <a href="/project/delete?id=${project.id}">DELETE</a>
            </td>
        </tr>
    </c:forEach>
</table>
<form action="/project/create">
    <button>CREATE PROJECT</button>
</form>
<jsp:include page="../include/_footer.jsp" />