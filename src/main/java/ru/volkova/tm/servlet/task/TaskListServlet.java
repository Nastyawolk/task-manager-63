package ru.volkova.tm.servlet.task;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ru.volkova.tm.repository.ProjectRepository;
import ru.volkova.tm.repository.TaskRepository;

@WebServlet("/tasks/*")
public class TaskListServlet extends HttpServlet {

    @Override
    protected void doGet(
            HttpServletRequest req, 
            HttpServletResponse resp
    ) throws ServletException, IOException {
        req.setAttribute("tasks", TaskRepository.getInstance().findAll());
        req.setAttribute("projectRepository", ProjectRepository.getInstance());
        req.getRequestDispatcher("/WEB-INF/views/task-list.jsp").forward(req, resp);
    }    

}
