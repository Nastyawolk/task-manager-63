package ru.volkova.tm.servlet.task;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ru.volkova.tm.repository.TaskRepository;

@WebServlet("/task/delete/*")
public class TaskDeleteServlet extends HttpServlet {

    @Override
    protected void doGet(
            HttpServletRequest req, 
            HttpServletResponse resp
    ) throws ServletException, IOException {
        final String id = req.getParameter("id");
        TaskRepository.getInstance().removeById(id);
        System.out.println(id);
        resp.sendRedirect("/tasks");
    }
    
}
