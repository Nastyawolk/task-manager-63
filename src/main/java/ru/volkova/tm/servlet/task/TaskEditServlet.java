package ru.volkova.tm.servlet.task;

import java.io.IOException;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.SneakyThrows;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.model.Task;
import ru.volkova.tm.repository.ProjectRepository;
import ru.volkova.tm.repository.TaskRepository;

@WebServlet("/task/edit/*")
public class TaskEditServlet extends HttpServlet {

    @Override
    protected void doGet(
            HttpServletRequest req, 
            HttpServletResponse resp
    ) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final Task task = TaskRepository.getInstance().findById(id);
        req.setAttribute("task", task);
        req.setAttribute("statuses", Status.values());
        req.setAttribute("projects", ProjectRepository.getInstance().findAll());
        req.getRequestDispatcher("/WEB-INF/views/task-edit.jsp").forward(req, resp);
    }

    @Override
    @SneakyThrows
    protected void doPost(
            HttpServletRequest req, 
            HttpServletResponse resp
    ) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final String name = req.getParameter("name");
        final String description = req.getParameter("description");
        final String statusValue = req.getParameter("status");
        final String projectId = req.getParameter("projectId");
        final Status status = Status.valueOf(statusValue);
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final String dateStartValue = req.getParameter("dateStart");
        final String dateFinishValue = req.getParameter("dateFinish");
        
        final Task task = new Task();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        task.setStatus(status);
        task.setProjectId(projectId);
        
        task.setDateStart(dateStartValue.isEmpty() ? null : dateFormat.parse(dateStartValue));
        task.setDateFinish(dateFinishValue.isEmpty() ? null : dateFormat.parse(dateFinishValue));
        
        TaskRepository.getInstance().save(task);
        resp.sendRedirect("/tasks");
    }  
    
}
