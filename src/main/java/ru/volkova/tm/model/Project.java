package ru.volkova.tm.model;

import java.util.Date;
import java.util.UUID;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.volkova.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public class Project {
    
    private String id = UUID.randomUUID().toString();
    
    private String name;
    
    private String description;
    
    private Status status = Status.NOT_STARTED;
    
    private Date dateStart = new Date();
    
    private Date dateFinish;

    public Project(String name) {
        this.name = name;
    }
    
    
    
}
