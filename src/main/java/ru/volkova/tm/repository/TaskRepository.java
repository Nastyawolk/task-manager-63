package ru.volkova.tm.repository;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import ru.volkova.tm.model.Task;

public class TaskRepository {
    
    private static final TaskRepository INSTANCE = new TaskRepository();
    
    public static TaskRepository getInstance() {
        return INSTANCE;
    }
    
    private Map<String, Task> tasks = new LinkedHashMap<>();
    
    {
        add(new Task("Task1"));
        add(new Task("Task2"));
    }
    
    public void add(Task task) {
        tasks.put(task.getId(), task);
    }
    
    public void create() {
        add(new Task("Task" + System.currentTimeMillis()));
    }
    
    public void save(Task task){
        tasks.put(task.getId(), task);
    }
    
    public Collection<Task> findAll() {
        return tasks.values();
    }
    
    public Task findById(String id) {
        return tasks.get(id);
    }
    
    public void removeById(String id) {
        tasks.remove(id);
    }

}
