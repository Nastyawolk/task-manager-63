package ru.volkova.tm.repository;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import ru.volkova.tm.model.Project;

public class ProjectRepository {
    
    private static final ProjectRepository INSTANCE = new ProjectRepository();
    
    public static ProjectRepository getInstance() {
        return INSTANCE;
    }
    
    private Map<String, Project> projects = new LinkedHashMap<>();
    
    {
        add(new Project("Project1"));
        add(new Project("Project2"));
        add(new Project("Project3"));
        add(new Project("Project4"));
    }
    
    public void add(Project project) {
        projects.put(project.getId(), project);
    }
    
    public void create() {
        add(new Project("Project" + System.currentTimeMillis()));
    }
    
    public void save(Project project){
        projects.put(project.getId(), project);
    }
    
    public Collection<Project> findAll() {
        return projects.values();
    }
    
    public Project findById(String id) {
        return projects.get(id);
    }
    
    public void removeById(String id) {
        projects.remove(id);
    }

}
