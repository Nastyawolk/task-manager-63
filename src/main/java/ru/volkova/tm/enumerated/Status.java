package ru.volkova.tm.enumerated;

public enum Status {
    
    NOT_STARTED("Not Started"),
    IN_PROGRESS("In Progress"),
    COMPLETE("Comlete");
    
    private final String displayName;

    private Status(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
    
}
