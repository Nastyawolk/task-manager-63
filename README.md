# TASK MANAGER WEB

## DEVELOPER INFO

**NAME**: Anastasia Volkova

**EMAIL**: aavolkova@t1-consulting.ru

**EMAIL**: nastya-wolk@mail.ru

## SOFTWARE

**JAVA**: OPENJDK 1.8

**OS**: Windows 10.0.17763.1577

## HARDWARE

**CPU**: i5-8250U

**RAM**: 16 GB

**SSD**: 512 GB

## BUILD PROGRAM

```
mvn clean install
```

## RUN PROGRAM

```
java -jar ./task-manager.jar
```
